﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MiniPaint
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            g = pnl_Draw.CreateGraphics();
            
        }

       
        bool startPaint = false;
        Graphics g;
        //nullable int for storing Null value
        int? initX = null;
        int? initY = null;
        bool drawSquare = false;
        
        //Event fired when the mouse pointer is moved over the Panel(pnl_Draw).
        private void pnl_Draw_MouseMove(object sender, MouseEventArgs e)
        {
            if (startPaint)
            {
                //Setting the Pen BackColor and line Width
                Pen p = new Pen(btn_PenColor.BackColor, float.Parse(cmb_PenSize.Text));
                //Drawing the line.
                g.DrawLine(p, new Point(initX ?? e.X, initY ?? e.Y), new Point(e.X, e.Y));
                initX = e.X;
                initY = e.Y;
            }
           
        }
        //Event Fired when the mouse pointer is over Panel and a mouse button is pressed
        private void pnl_Draw_MouseDown(object sender, MouseEventArgs e)
        {
            startPaint = true;
            if (drawSquare)
            {
                //Use Solid Brush for filling the graphic shapes
                SolidBrush sb = new SolidBrush(btn_PenColor.BackColor);
                //setting the width and height same for creating square.
                //Getting the width and Heigt value from Textbox(txt_ShapeSize)
                g.FillRectangle(sb, e.X, e.Y, int.Parse(txt_ShapeSize.Text), int.Parse(txt_ShapeSize.Text));
                //setting startPaint and drawSquare value to false for creating one graphic on one click.
                startPaint = false;
                drawSquare = false;
            }
            
        }
        //Fired when the mouse pointer is over the pnl_Draw and a mouse button is released.
        private void pnl_Draw_MouseUp(object sender, MouseEventArgs e)
        {
            startPaint = false;
            initX = null;
            initY = null;
        }
        //Button for Setting pen Color
        private void button1_Click(object sender, EventArgs e)
        {
            //Open Color Dialog and Set BackColor of btn_PenColor if user click on OK
            ColorDialog c = new ColorDialog();
            if(c.ShowDialog()==DialogResult.OK)
            {
                btn_PenColor.BackColor = c.Color;
            }
        }
        //New 
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Clearing the graphics from the Panel(pnl_Draw)
            g.Clear(pnl_Draw.BackColor);
            //Setting the BackColor of pnl_draw and btn_CanvasColor to White on Clicking New under File Menu
            pnl_Draw.BackColor = Color.White;
            btn_CanvasColor.BackColor = Color.White;
        }
       //Setting the Canvas Color
        private void btn_CanvasColor_Click_1(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            if(c.ShowDialog()==DialogResult.OK)
            {
                pnl_Draw.BackColor = c.Color;
                btn_CanvasColor.BackColor = c.Color;
            }
        }

        private void btn_Square_Click(object sender, EventArgs e)
        {
            
            drawSquare = true;
            
            
        }

        
        //Exit under File Menu
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Do you want to Exit?","Exit",MessageBoxButtons.YesNo,MessageBoxIcon.Information)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        //About under Help Menu
        private void aboutMiniPaintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private void cmb_PenSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        
        //Incializa un componente SaveFileDialog.
                SaveFileDialog saveFileDialog = new SaveFileDialog();
        //Cuando buscas archivos te muestra todos los .bmp.
        saveFileDialog.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
                //Titulo
                saveFileDialog.Title = "Guardar gráfico como imagen";
            // preguntamos si elegiste un nombre de archivo.
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //Extención del archivo por defecto segun el filtro del saveFileDialog
                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        saveFileDialog.DefaultExt = "jpg";
                        break;

                    case 2:
                        saveFileDialog.DefaultExt = "bmp";
                        break;

                    case 3:
                        saveFileDialog.DefaultExt = "gif";
                        break;
                }

                //Obtenemos alto y ancho del panel
                int width = pnl_Draw.Width;
                int height = pnl_Draw.Height;
                //Inicializamos un objeto BitMap con las dimensiones del Panel
                Bitmap bitMap = new Bitmap(width, height);
                //Inicializamos un objeto Rectangle en la posicion 0,0 y con dimensiones iguales a las del panel.
                //0,0 y las mismas dimensiones del panel porque queremos tomar todo el panel
                // o si solo queremos tomar una parte pues podemos dar un punto de inicio diferente y dimensiones distintas.
                Rectangle rec = new Rectangle(0, 0, width, height);
                //Este metodo hace la magia de copiar las graficas a el objeto Bitmap
                pnl_Draw.DrawToBitmap(bitMap, rec);
                // Y por ultimo salvamos el archivo pasando como parametro el nombre que asignamos en el saveDialogFile
                bitMap.Save(saveFileDialog.FileName);
            }
                }
    }
}
